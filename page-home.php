<?php
/*
 Template Name: Home Page Template
*/
?>

<?php get_header(); ?>

<div id="content">

	<div id="inner-content" class="wrap cf">

		<main id="main" class="grid-2" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

						<div id="app"></div>

					</article>

			<?php endwhile;
			endif; ?>

			<?= get_template_part('partials/weather', 'app') ?>

		</main>

	</div>

</div>


<?php get_footer(); ?>