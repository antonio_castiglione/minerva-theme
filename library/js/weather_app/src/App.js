import React from "react";
import Weather from "./components/Weather";

// State provider
import { GlobalProvider } from "./context/GlobalState";

function App() {
  return (
    <GlobalProvider>
      <div className="app">
        <Weather />
      </div>
    </GlobalProvider>
  );
}

export default App;
