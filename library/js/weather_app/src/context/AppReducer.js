export default (state, action) => {
  switch (action.type) {
    case "GEO_DATA":
      return {
        ...state,
        coords: action.payload,
      };
    case "GEO_FAIL":
      return {
        ...state,
        coords: action.payload,
      };
    default:
      return state;
  }
};
