import React, { createContext, useReducer } from "react";
import AppReducer from "./AppReducer";

// Initial state
const initialState = {
  coords: {},
};

// Create context
export const GlobalContext = createContext(initialState);

// Provider component
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  // Actions
  function getGeolocationData() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          dispatch({
            type: "GEO_DATA",
            payload: {
              lat: position.coords.latitude,
              lon: position.coords.longitude,
            },
          });
        },
        function (error) {
          dispatch({
            type: "GEO_FAIL",
            payload: { err: "Geolocation service cannot be reached" },
          });
        }
      );
    } else {
      dispatch({
        type: "GEO_FAIL",
        payload: { err: "Geolocation service cannot be reached" },
      });
    }
  }

  return (
    <GlobalContext.Provider
      value={{
        coords: state.coords,
        getGeolocationData,
      }}>
      {children}
    </GlobalContext.Provider>
  );
};
