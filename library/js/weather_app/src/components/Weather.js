import React, { useState, useContext, useEffect } from "react";
import { GlobalContext } from "../context/GlobalState";

const api = {
  key: "7fe0f2779046e62e0e701d6915551671",
  base: "https://api.openweathermap.org/data/2.5/",
};

function Weather() {
  const [query, setQuery] = useState("");
  const [weather, setWeather] = useState("");

  // Fetch coords from global state
  const { coords, getGeolocationData } = useContext(GlobalContext);

  useEffect(() => {
    getGeolocationData();
  }, []);

  const search = (evt) => {
    if (evt.key === "Enter") {
      fetch(`${api.base}weather?q=${query}&units=imperial&APPID=${api.key}`)
        .then((res) => res.json())
        .then((result) => {
          setWeather(result);
          setQuery("");
        });
    }
  };

  const defaultLocation = () => {
    if (Object.keys(coords).length > 0 && !coords.err) {
      fetch(
        `${api.base}weather?lat=${coords.lat}&lon=${coords.lon}&units=imperial&APPID=${api.key}`
      )
        .then((res) => res.json())
        .then((result) => {
          setWeather(result);
        });
    } else {
      return;
    }
  };

  const dateBuilder = (d) => {
    let months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    return `${day} ${month} ${date} ${year}`;
  };

  return (
    <div
      className={
        typeof weather.main != "undefined"
          ? weather.main.temp > 60
            ? "weather warm"
            : "weather"
          : "weather"
      }>
      <div className="weather__container">
        <div className="weather__search">
          <input
            type="text"
            className="weather__input"
            onChange={(e) => setQuery(e.target.value)}
            value={query}
            onKeyPress={search}
            placeholder="Search by city name..."
          />
        </div>
        {typeof weather.main != "undefined" ? (
          <React.Fragment>
            <div className="weather__location-box">
              <div className="location">
                {weather.name}, {weather.sys.country}
              </div>
              <div className="date">{dateBuilder(new Date())}</div>
            </div>
            <div className="weather__box">
              <div className="temp">{Math.round(weather.main.temp)} °F</div>
              <div className="status">{weather.weather[0].main}</div>
            </div>
          </React.Fragment>
        ) : weather.cod === "404" ? (
          <React.Fragment>
            <div className="weather__location-box">
              <h2 className="location">No Location Found</h2>
            </div>
            <div className="weather__box">
              <div className="error">
                The search parameter provided was invalid
              </div>
            </div>
          </React.Fragment>
        ) : defaultLocation() ? (
          <React.Fragment>
            <div className="weather__location-box">
              <div className="location">
                {weather.name}, {weather.sys.country}
              </div>
              <div className="date">{dateBuilder(new Date())}</div>
            </div>
            <div className="weather__box">
              <div className="temp">{Math.round(weather.main.temp)} °F</div>
              <div className="status">{weather.weather[0].main}</div>
            </div>
          </React.Fragment>
        ) : null}
      </div>
    </div>
  );
}

export default Weather;
