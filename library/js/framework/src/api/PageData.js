import React, { useEffect, useState } from "react";

const pagesUrl = `${window.location.origin}/wp-json/wp/v2/pages/?per_page=100`;

const PageData = () => {
  const [page, setPage] = useState("");

  useEffect(() => {
    getCurrentPage();
  }, []);

  const getCurrentPage = async () => {
    let api = await fetch(pagesUrl).then((res) => res.json());
    let reg = /\//gi;
    let currentPage;
    if (window.location.pathname == "/") {
      // home page must contain the front in the slug
      currentPage = "front";
    } else if (window.location.pathname.split("/")[2] !== "") {
      // catching 2nd lvl pages
      currentPage = window.location.pathname.split("/")[2];
    } else {
      // for all other pages
      currentPage = window.location.pathname.replace(reg, "");
    }

    let currentPageData = api.filter(
      (el) => el.slug.includes(currentPage) == true
    );
    setPage(currentPageData[0]);
  };

  return (
    <section>
      {typeof page.title != "undefined" ? (
        <React.Fragment>
          <header>
            <h1>{page.title.rendered}</h1>
          </header>
          <div
            className="entry-content"
            dangerouslySetInnerHTML={{ __html: page.content.rendered }}></div>
        </React.Fragment>
      ) : null}
    </section>
  );
};

export default PageData;
