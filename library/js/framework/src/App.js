import React from "react";
import PageData from "./api/PageData";

function App() {
  return (
    <div className="App">
      <PageData />
    </div>
  );
}

export default App;
